package com.t1.yd.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

    void showById();

    void showByIndex();

    void removeById();

    void removeByIndex();

    void updateById();

    void updateByIndex();

}
