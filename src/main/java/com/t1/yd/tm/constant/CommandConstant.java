package com.t1.yd.tm.constant;

public class CommandConstant {

    public static final String ABOUT = "about";

    public static final String VERSION = "version";

    public static final String HELP = "help";

    public static final String INFO = "info";

    public static final String EXIT = "exit";

    public static final String COMMANDS = "commands";

    public static final String ARGUMENTS = "arguments";

    public static final String PROJECT_LIST = "project_list";

    public static final String PROJECT_CLEAR = "project_clear";

    public static final String PROJECT_CREATE = "project_create";

    public static final String PROJECT_GET_BY_ID = "project_get_by_id";

    public static final String PROJECT_GET_BY_INDEX = "project_get_by_index";

    public static final String PROJECT_REMOVE_BY_ID = "project_remove_by_id";

    public static final String PROJECT_REMOVE_BY_INDEX = "project_remove_by_index";

    public static final String PROJECT_UPDATE_BY_ID = "project_update_by_id";

    public static final String PROJECT_UPDATE_BY_INDEX = "project_update_by_index";

    public static final String TASK_LIST = "task_list";

    public static final String TASK_CREATE = "task_create";

    public static final String TASK_GET_BY_ID = "task_get_by_id";

    public static final String TASK_GET_BY_INDEX = "task_get_by_index";

    public static final String TASK_REMOVE_BY_ID = "task_remove_by_id";

    public static final String TASK_REMOVE_BY_INDEX = "task_remove_by_index";

    public static final String TASK_UPDATE_BY_ID = "task_update_by_id";

    public static final String TASK_UPDATE_BY_INDEX = "task_update_by_index";

    public static final String TASK_CLEAR = "task_clear";

}
