package com.t1.yd.tm.controller;

import com.t1.yd.tm.api.IProjectController;
import com.t1.yd.tm.api.IProjectService;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");

        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();

        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.create(name, description);
        if (project == null) {
            System.out.println("[ERROR]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();

        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }

        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("[ENTER ID:]");

        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);

        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }

        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showByIndex() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("[ENTER INDEX:]");

        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(index);

        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }

        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void removeById() {
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeById(id);
        if (project == null) System.out.println("[FAIL]");
        System.out.println("[OK]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("[UPDATE PROJECT BY ID]");

        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();

        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();

        System.out.println("[ENTER DESCRIPTION:]");
        final String desc = TerminalUtil.nextLine();

        final Project project = projectService.updateById(id, name, desc);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void updateByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");

        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber();

        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();

        System.out.println("[ENTER DESCRIPTION:]");
        final String desc = TerminalUtil.nextLine();

        final Project project = projectService.updateByIndex(index, name, desc);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESC: " + project.getDescription());
    }

}
