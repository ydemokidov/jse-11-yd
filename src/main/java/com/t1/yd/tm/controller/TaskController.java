package com.t1.yd.tm.controller;

import com.t1.yd.tm.api.ITaskController;
import com.t1.yd.tm.api.ITaskService;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");

        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();

        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();

        final Task task = taskService.create(name, description);
        if (task == null) {
            System.out.println("[ERROR]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();

        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }

        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("[ENTER ID:]");

        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);

        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }

        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void showByIndex() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("[ENTER INDEX:]");

        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);

        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }

        showTask(task);
        System.out.println("[OK]");
    }

    @Override
    public void removeById() {
        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) System.out.println("[FAIL]");
        System.out.println("[OK]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void updateById() {
        System.out.println("[UPDATE TASK BY ID]");

        System.out.println("[ENTER ID:]");
        final String id = TerminalUtil.nextLine();

        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();

        System.out.println("[ENTER DESCRIPTION:]");
        final String desc = TerminalUtil.nextLine();

        final Task task = taskService.updateById(id, name, desc);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void updateByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");

        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber();

        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();

        System.out.println("[ENTER DESCRIPTION:]");
        final String desc = TerminalUtil.nextLine();

        final Task task = taskService.updateByIndex(index, name, desc);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
    }

    private void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESC: " + task.getDescription());
    }
}
